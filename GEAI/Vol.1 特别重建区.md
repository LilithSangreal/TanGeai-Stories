# Vol.1 特别重建区

看过了医生之后，我们乘坐着『特9』特别特快线，穿过连绵而环绕成圆的山脉，驶向中心，最后在一个近乎崭新的站台停下，这里便是我们的新家。

站台虽然崭新，但局促，是钢铁浇筑的机械框架快速拼装成的一个密闭的半球体。就仿佛呼应这气氛似的，车上的乘客少的可怜。

接连几声特殊的脆响，是车停靠站台的标志，机械安全手臂锁紧了车厢的两侧。随即便有好听的女声传来，提醒乘客下车。不论这个声音多么温暖迷人，值得一提的是，这辆自动驾驶的特快线上，并没有任何的乘务人员。

我轻轻推醒在一边发出轻微鼾声的爸爸。他合上了半张的、像鱼般缓缓开合的嘴，牵起了我的手作为回应。

距离下车的车门不到一千米，便是这个球形的，密不透风的车站的边缘。在这个边缘，两扇门并列地站着，一扇巨大，高耸，无数钢筋和齿轮保证着这扇门的威严。而另一扇则微小得只容一人通过，在站台的灯光下闪着特殊合金的光芒。两扇门并列站着，就好像现在站在它们面前的我和爸爸。

我们径直走向小巧的门前，门上的虹膜检测装置兀自散发着两束幽蓝的光。虽然来到这里的时日不多，但已经是习以为常的光景。

「工号：3029，特殊重建科学家。已确认。」

这就是车上的乘客总是寥寥无几的原因，如果没有这个特殊的工号，人们几乎只能乘坐着一辆特别的铁道列车，在特定的时间，从旁边的大门出入车站。即使这个车站是这个『第九特殊重建区』唯一和外界沟通的渠道。

『第九特殊重建区』。

我们来到这个地方居住已经有将近一个月了。在那之前，日本的住所里，爸爸背着巨大的双肩包带着平和的表情回到家里，一如往常。

「收拾东西吧，歌爱，我们要搬家了。」

爸爸半蹲着，对正在看电视的我笑着说。

然而气氛却有所不同。爸爸从来也不会对我使用命令的口气，尽管我并不讨厌执行命令。

我们坐上了回国的飞机，接着便来到了这里。

我拥有过目不忘的记忆力，在之前就读的小学里，老师要求背诵的文章，我只需要阅读一遍便可以记住——不过前提是我能够认出所有的文字。我的阅读有一些障碍，有些书上的汉字和假名，无论怎样我都无法清除地认出。爸爸说我身上有很多的病，这只是其中的一种。但总体来说，我的记忆力还是非常了不起的。回国之后的一个星期内，我便基本掌握了从来没有接触过的中文。

但即使是这样的记忆力，我也没办法准确描述这所谓的『第九特殊重建区』究竟位于这国家的哪个位置。正如名字描述的那样，被称作重建区的地方不只有一个，他们相互之间以特殊铁道相连，外观也相似，都是成环的山脉包围的盆地。  

不过这里究竟发生过什么，在网络上，众多的残缺的、说法不一的文献记录中却或多或少的有所提及——那场战争。

那场如闪电般转瞬即逝的战争，在地球的各处留下的疮痍和辐射让大片的土地成为没有生命的荒土，而爸爸则是由政府直接安排前来从事战后的重建工作。根据不同的需要，医生，工人，从事在各行各业的专家被派遣而来，有些具有特殊地位的，比如作为计算机科学家的爸爸，会拥有一个特殊的工号，这个工号会提供使用重建区各种基础设施的权限。而大多数不拥有工号的人，将会由国安部直属的管理人员统一管理，由于重建工作涉密，所以这一部分人的出入被严格限制，有必要的时候才会统一乘坐公车前往车站搭乘特别特快线。

我们作为这里的新居民，自然是没有见过他们的。不过可以想到，我们现在住处周围完善的基础设施，应该都是这些人亲手建造的。

我们居住在居民区的一幢独栋别墅中，虽然家装风格无论怎样也算不上时髦，样板房一般。但已经可以称得上是豪华了，来自国营科技公司的最新净水仪器的纯净水和风力电提供了日常生活的卫生和便利，附近超市优惠价格的转基因蔬菜肉类保证了一日三餐的营养。这一切都是免费提供的，是根据重建工作人员将会对重建工作的贡献度提供的福利。

从车站的大门走出来，便是豁然开朗。崭新整齐的街道和房屋被蔚蓝如洗的明亮“天空”笼罩着。

这并不是真正的天空，而是用无缝连接的LED巨幕模拟的天空图像。就连铺洒下来的温暖日光也只是遍布各处的核聚变日光发生器散发的——为了防止居民接触不到阳光而患病。但看到这样的景色，我还是禁不住地安心。

毕竟我们作为这个城镇的第一批居民，已经在这个城镇生活了将近半年，这个城镇如果说有任何的变化，那都是我们生活过的痕迹。

爸爸开启了路旁停靠的电动汽车，并帮我安置好了后备箱里承载着的儿童安全座椅，确定我已经系好安全带后，便发动汽车载着我向着家的方向驶去。

“呐，爸爸，我什么时候才能在去上学啊？”

“……嗯，现在的话，姑且可能需要再等等。”

这个问题，我不是第一次问了，每次得到的回答都是相似的。从日本回来之后，我就再也没有上学。虽然也并不是非常寂寞，但是有的时候还是会想起在日本的好朋友——不知道莉奈现在在做什么呢，是不是还是每天和伏见君吵架呢？这样想着的我，看着前方流动着的、有些一成不变的景色，不禁出了神。然而爸爸的一句话却突然打断了我的回忆。

“歌爱，是不是觉得有些寂寞呢？”

“没有哦，也没有特别感到寂寞……”

虽然是发自内心的话，但爸爸似乎认为我是因为体贴才这么说的，于是兀自地提了一个建议：

“——明天带你去见李叔叔，我们来学唱歌好不好？“

唱歌吗？其实还没有尝试过，感到新鲜的我情不自禁地笑了出来。

”好！“

李志浩叔叔是爸爸的挚友，也是爸爸的大学同学。现在在第五重建区做电子工程师的工作。虽然他是电子工程师，但爸爸说我身上的一些病只有李叔叔才能帮助我治疗。因此我们基本上每个月都要去拜访李叔叔几次。

爸爸从来没有提及过他和李叔叔的学生时代。他们十几年的好友，默契得甚至不用言语就能传达心里的想法，想必是共同经历了许多故事吧。

那必然是一个漫长的故事。
